package az.ingress.studentcrudwithdockercompose.controller;


import az.ingress.studentcrudwithdockercompose.model.entity.Account;
import az.ingress.studentcrudwithdockercompose.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {


    private final AccountService accountService;

    @PutMapping("/update/{accountId}/{amount}")
    public Account updateAccountBalance(@PathVariable Long accountId,@PathVariable Long amount) {
        return accountService.setAccountBalance(accountId, amount);
    }

    @PutMapping("/wait/update/{accountId}/{amount}")
    public Account updateAccountBalance2(@PathVariable Long accountId,@PathVariable Long amount) throws Exception {
        return accountService.setAccountBalance2(accountId, amount);
    }

    @GetMapping("/{id}")
    public Account getAccountById(@PathVariable Long id) {
        return accountService.getAccountById(id);
    }


}
