package az.ingress.studentcrudwithdockercompose.controller;

import az.ingress.studentcrudwithdockercompose.model.dto.StudentRequest;
import az.ingress.studentcrudwithdockercompose.model.dto.StudentResponse;
import az.ingress.studentcrudwithdockercompose.model.entity.Student;
import az.ingress.studentcrudwithdockercompose.model.mapper.StudentMapper;
import az.ingress.studentcrudwithdockercompose.repository.StudentRepository;
import az.ingress.studentcrudwithdockercompose.service.StudentService;
import az.ingress.studentcrudwithdockercompose.specification.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    private String s = "MartinDavid";

    @GetMapping("/cache/{name}")
    public Student getStudentByName(@PathVariable("name") String name) {
        return studentService.findByName(name);
    }


//    @GetMapping("/spec")
//    public List<Student> getStudentsBySpecification(@RequestBody List<SearchCriteria> searchCriteria) {
//        return studentService.findAllWithSpecification(searchCriteria);
//    }

    @PutMapping("/{id}")
    public Student updateStudent(@RequestBody StudentRequest studentRequest, @PathVariable("id") Long id) {
        return studentService.updateStudent(studentRequest, id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentResponse> getStudentById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(studentService.getStudentById(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<StudentResponse>> getAllStudents() {
        return new ResponseEntity<>(studentService.getAllStudents(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<StudentResponse> addStudent(@RequestBody StudentRequest studentRequest) {
        return new ResponseEntity<>(studentService.addStudent(studentRequest), HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<StudentResponse> deleteStudentById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(studentService.deleteStudentById(id), HttpStatus.OK);
    }

    private boolean isStudentExist(Long id) {
        return studentRepository.existsById(id);
    }
}
