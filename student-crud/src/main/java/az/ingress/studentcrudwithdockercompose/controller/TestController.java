package az.ingress.studentcrudwithdockercompose.controller;

import az.ingress.studentcrudwithdockercompose.model.entity.User;
import az.ingress.studentcrudwithdockercompose.service.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
@Slf4j
public class TestController {


    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;

    @GetMapping("/admin/secure")
    public String test() {
        return "Hello from Secure API for Admins";
    }

    @GetMapping("/user/secure")
//    @PreAuthorize("hasAuthority('USER')")
    public String test1() {
        return "Hello from Secure API for Users";
    }

    @GetMapping("/forbidden")
    public String test2() {
        return "Hello from forbidden test API";
    }

    @GetMapping
    public String test3() {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        "admin",
                        "admin"
                )
        );
        return "Hello from Secure API for everyone";
    }


    @GetMapping("/unsecure")
    public String token() {
        UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
        String token = jwtService.generateToken((User) userDetails);
        log.info("Token: {}", token);
        return "Token: " + token;
    }

}
