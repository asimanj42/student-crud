package az.ingress.studentcrudwithdockercompose.service;

import az.ingress.studentcrudwithdockercompose.exception.type.ResourceNotFoundException;
import az.ingress.studentcrudwithdockercompose.model.dto.StudentRequest;
import az.ingress.studentcrudwithdockercompose.model.dto.StudentResponse;
import az.ingress.studentcrudwithdockercompose.model.entity.Student;
import az.ingress.studentcrudwithdockercompose.model.mapper.StudentMapper;
import az.ingress.studentcrudwithdockercompose.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    @Override
    public StudentResponse getStudentById(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Student", "id", id));
        return studentMapper.mapEntityToStudentResponse(student);
    }



    @Override
    @Cacheable(cacheNames = "student", key = "#name")
    public Student findByName(String name) {
        return studentRepository.findByName(name).get();
    }

    @Override
    @CachePut(cacheNames = "student", key = "#id")
    public Student updateStudent(StudentRequest studentRequest, Long id) {
        StudentResponse studentResponse = getStudentById(id);
        Student updatedStudent = updateIfNotNull(studentMapper.mapStudentResponseToEntity(studentResponse), studentRequest);
        updatedStudent.setId(id);
        return studentRepository.save(updatedStudent);
    }

    @Override
    public List<StudentResponse> getAllStudents() {
        List<Student> studentList = studentRepository.findAll();
        return studentMapper.mapEntityListToStudentResponseList(studentList);
    }

    @Override
    public StudentResponse addStudent(StudentRequest studentRequest) {
        Student student = studentMapper.mapStudentRequestToEntity(studentRequest);
        Student savedStudent = studentRepository.save(student);
        return studentMapper.mapEntityToStudentResponse(savedStudent);
    }



    @Override
    public StudentResponse deleteStudentById(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Student", "id", id));
        studentRepository.deleteById(id);
        return studentMapper.mapEntityToStudentResponse(student);
    }

    public Student updateIfNotNull(Student student, StudentRequest studentRequest) {
        if (studentRequest.getName() != null) {
            student.setName(studentRequest.getName());
        }
        if (studentRequest.getSurname() != null) {
            student.setSurname(studentRequest.getSurname());
        }
        if (studentRequest.getEmail() != null) {
            student.setEmail(studentRequest.getEmail());
        }
        if (studentRequest.getGpa() != null) {
            student.setGpa(studentRequest.getGpa());
        }
        return student;
    }

}
