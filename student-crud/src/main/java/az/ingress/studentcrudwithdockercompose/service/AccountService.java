package az.ingress.studentcrudwithdockercompose.service;

import az.ingress.studentcrudwithdockercompose.model.entity.Account;
import az.ingress.studentcrudwithdockercompose.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Account setAccountBalance(Long accountId, Long amount) {

        Account account = accountRepository.findById(accountId).get();
        account.setBalance(account.getBalance() - amount);
        return accountRepository.save(account);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Account setAccountBalance2(Long accountId, Long amount) throws Exception {

        Account account = accountRepository.findById(accountId).get();
        account.setBalance(account.getBalance() - amount);
        Thread.sleep(5000);
        return accountRepository.save(account);
    }

    public Account getAccountById(Long id) {
        return accountRepository.findById(id).get();
    }
}
