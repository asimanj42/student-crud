package az.ingress.studentcrudwithdockercompose.service;

import az.ingress.studentcrudwithdockercompose.model.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.function.Function;

@Service
public class JwtService {

    private String SECRET_KEY = "ZnV0dXJlIGJhY2tlbmQgZW5naW5lZXIgamF2YSBzcHJpbmcgYm9vdCBrOHMgY2ljZCBkb2NrZXIgc3FsIGthZmthIG1pY3Jvc2VydmljZQ==";

    public Claims parseToken(String token) {
        return Jwts.parser()
                .verifyWith(getSigningKey(SECRET_KEY))
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

    public String generateToken(User user) {
        return Jwts.builder()
                .subject(user.getUsername())
                .claim("authorities", user.getAuthorities())
                .expiration(Date.from(Instant.now().plus(Duration.ofMinutes(5))))
                .signWith(getSigningKey(SECRET_KEY))
                .compact();
    }

    public  <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = parseToken(token);
        return claimsResolver.apply(claims);
    }

    private SecretKey getSigningKey(String secretKey) {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey));
    }

}
