package az.ingress.studentcrudwithdockercompose.service;

import az.ingress.studentcrudwithdockercompose.model.dto.StudentRequest;
import az.ingress.studentcrudwithdockercompose.model.dto.StudentResponse;
import az.ingress.studentcrudwithdockercompose.model.entity.Student;
import az.ingress.studentcrudwithdockercompose.specification.SearchCriteria;

import java.util.List;

public interface StudentService {
    StudentResponse getStudentById(Long id);


    Student findByName(String name);

    List<StudentResponse> getAllStudents();

    StudentResponse addStudent(StudentRequest studentRequest);

    Student updateStudent(StudentRequest studentRequest, Long id);

    StudentResponse deleteStudentById(Long id);
}
