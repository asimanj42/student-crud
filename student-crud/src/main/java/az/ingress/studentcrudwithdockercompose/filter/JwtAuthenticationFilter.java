package az.ingress.studentcrudwithdockercompose.filter;

import az.ingress.studentcrudwithdockercompose.exception.error.ErrorResponse;
import az.ingress.studentcrudwithdockercompose.service.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;

@Component
@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    public static final String BEARER_ = "Bearer ";
    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;
    private final ObjectMapper objectMapper;

    // Filterde ne yaziriq: tokeni gotururuk
    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain)
            throws ServletException, IOException {

        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        try {
            if (authHeader == null || !authHeader.startsWith(BEARER_)) {
                filterChain.doFilter(request, response);
                return;
            }
            String token = authHeader.substring(BEARER_.length());
            Date expiration = jwtService.extractClaim(token, Claims::getExpiration);
            if (expiration.before(new Date())) {
                throw new RuntimeException("Token is expired");
            }
            //tokendenki usernamei(claim) gotururuk
            String username = jwtService.extractClaim(token, Claims::getSubject);
            SecurityContextHolder.getContext().setAuthentication(getAuthentication(username));

        } catch (SignatureException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            ErrorResponse error = getErrorResponse(request, e);
            response.getWriter().write(objectMapper.writeValueAsString(error));
            return;
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ErrorResponse error = getErrorResponse(request, e);
            response.getWriter().write(objectMapper.writeValueAsString(error));
            return;
        }

        filterChain.doFilter(request, response);

    }

    private ErrorResponse getErrorResponse(HttpServletRequest request, Exception e) {
        return ErrorResponse.builder()
                .message("Qaqas tokeni yoxla, sehv gonderirsen")
                .status(HttpStatus.UNAUTHORIZED)
                .timestamp(LocalDateTime.now())
                .path(request.getRequestURI())
                .build();
    }

    /*
     Username password filterden kecmek ucun authentication lazimdir
     Authentication obyektini bu metodda dolduruq
     Asagidaki metodda ise contexe set edirik. Artiq elimizde authentication var, demeli request ve response-u novbeti filtere oturmek vaxtidir.
     SecurityContextHolder.getContext().setAuthentication(getAuthentication(username));
     */
    private Authentication getAuthentication(String username) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return new UsernamePasswordAuthenticationToken(username, null, userDetails.getAuthorities());
    }
}
