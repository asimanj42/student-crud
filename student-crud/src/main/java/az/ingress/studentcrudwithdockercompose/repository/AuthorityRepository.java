package az.ingress.studentcrudwithdockercompose.repository;

import az.ingress.studentcrudwithdockercompose.model.entity.Authority;
import az.ingress.studentcrudwithdockercompose.model.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
