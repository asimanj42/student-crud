package az.ingress.studentcrudwithdockercompose.repository;

import az.ingress.studentcrudwithdockercompose.model.entity.Account;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long>{

    @Override
    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<Account> findById(Long aLong);
}
