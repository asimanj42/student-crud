package az.ingress.studentcrudwithdockercompose.specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

public enum SearchOperation {

    GREATER_THAN {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.greaterThan(root.get(criteria.getKey()), criteria.getValue().toString());
        }
    },
    LESS_THAN {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.lessThan(root.get(criteria.getKey()), criteria.getValue().toString());
        }
    },
    GREATER_THAN_EQUAL {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.greaterThanOrEqualTo(root.get(criteria.getKey()), criteria.getValue().toString());
        }
    },
    LESS_THAN_EQUAL {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.lessThanOrEqualTo(root.get(criteria.getKey()), criteria.getValue().toString());
        }
    },
    NOT_EQUAL {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.notEqual(root.get(criteria.getKey()), criteria.getValue().toString());
        }
    },
    EQUAL {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.equal(root.get(criteria.getKey()),
                    castToRequiredType(root.get(criteria.getKey()).getJavaType(), criteria.getValue()));
        }
    },
    MATCH {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.like(builder.lower(root.get(criteria.getKey())),
                    PERCENT_SIGN + criteria.getValue().toString().toLowerCase() + PERCENT_SIGN);
        }
    },
    IN {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.in(root.get(criteria.getKey())).value(criteria.getValue());
        }
    },
    NOT_IN {
        @Override
        public Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder) {
            return builder.not(root.get(criteria.getKey())).in(criteria.getValue());
        }
    };
    private static final String PERCENT_SIGN = "%";

    public abstract Predicate buildPredicate(Root<?> root, SearchCriteria criteria, CriteriaBuilder builder);

    public Object castToRequiredType(Class fieldType, Object value) {
        if (Enum.class.isAssignableFrom(fieldType)) {
            return Enum.valueOf(fieldType, value.toString());
        }
        return value;
    }
}


