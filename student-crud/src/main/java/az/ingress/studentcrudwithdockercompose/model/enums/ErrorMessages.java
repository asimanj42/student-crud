package az.ingress.studentcrudwithdockercompose.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorMessages {

    UNAUTHORIZED("Zəhmət olmasa login olun");

    private final String message;
}
