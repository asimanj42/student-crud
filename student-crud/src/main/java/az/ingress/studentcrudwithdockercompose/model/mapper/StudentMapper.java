package az.ingress.studentcrudwithdockercompose.model.mapper;

import az.ingress.studentcrudwithdockercompose.model.dto.StudentRequest;
import az.ingress.studentcrudwithdockercompose.model.dto.StudentResponse;
import az.ingress.studentcrudwithdockercompose.model.entity.Student;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StudentMapper {

    public StudentResponse mapEntityToStudentResponse(Student student) {
        return StudentResponse.builder()
                .id(student.getId())
                .name(student.getName())
                .surname(student.getSurname())
                .email(student.getEmail())
                .gpa(student.getGpa())
                .build();
    }

    public Student mapStudentResponseToEntity(StudentResponse studentResponse) {
        return Student.builder()
                .id(studentResponse.getId())
                .name(studentResponse.getName())
                .surname(studentResponse.getSurname())
                .email(studentResponse.getEmail())
                .gpa(studentResponse.getGpa())
                .build();
    }

    public Student mapStudentRequestToEntity(StudentRequest studentRequest) {
        return Student.builder()
                .name(studentRequest.getName())
                .surname(studentRequest.getSurname())
                .email(studentRequest.getEmail())
                .gpa(studentRequest.getGpa())
                .build();
    }

    public List<StudentResponse> mapEntityListToStudentResponseList(List<Student> studentList) {
        return studentList.stream()
                .map(this::mapEntityToStudentResponse)
                .collect(Collectors.toList());
    }

}
