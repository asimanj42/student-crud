package az.ingress.studentcrudwithdockercompose;

import az.ingress.studentcrudwithdockercompose.model.entity.Authority;
import az.ingress.studentcrudwithdockercompose.model.entity.Student;
import az.ingress.studentcrudwithdockercompose.model.entity.User;
import az.ingress.studentcrudwithdockercompose.repository.AuthorityRepository;
import az.ingress.studentcrudwithdockercompose.repository.StudentRepository;
import az.ingress.studentcrudwithdockercompose.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StudentCrudApplication implements CommandLineRunner {

    UserRepository userRepository;
    AuthorityRepository authorityRepository;
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(StudentCrudApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Authority admin = new Authority();
        admin.setAuthority("ADMIN");


//        User user = User.builder()
//                .username("admin")
//                .password(passwordEncoder.encode("admin"))
//                .enabled(true)
//                .accountNonExpired(true)
//                .accountNonLocked(true)
//                .credentialsNonExpired(true)
//                .authorities(Set.of(admin))
//                .build();

        User user = new User();
        user.setUsername("admin");
        user.setPassword(passwordEncoder.encode("admin"));
        user.setEnabled(true);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setAuthorities(Set.of(admin));


        userRepository.save(user);


    }
}
